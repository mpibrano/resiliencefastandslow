function Video_ShortTermDynamics(results,filename)

    global dirData

    params     = results.params;
    n          = params.n;
        
    y_snapshot         = results.y_snapshot;
    markalive_snapshot = results.markalive_snapshot;
    t_snapshot         = results.t_snapshot;
    
    
    filename = [dirData,filename];

    hP = figure('Units', 'centimeters','Position', [0, 0,6,5],...
       'PaperUnits', 'centimeters', 'PaperSize', [6,5],...
       'PaperPositionMode','Auto','color','white');
   
    left_alignment = 1;
    top_alignment = 0.5;
    axs = [];
    axs_colorbar = [];
    axs_ana = [];
    w = 3.5;
    w_spacing = 4.2;
    w_spacing_vertical = 5;
    for i1 = 1:1   
        top  = top_alignment - w_spacing_vertical*(i1-1);

        left = 1;
        axs_colorbar(i1) = axes('Units', 'centimeters','Position', [left top w*0.2 w]);    

        for i2 = 1:1
            left = left_alignment + w_spacing*(i2-1) + 1.;    
            axs(i1,i2) = axes('Units', 'centimeters','Position', [left top w w]);
        end        

       % left = left_alignment + w_spacing*1+2.2;
       % axs_ana(i1)  = axes('Units', 'centimeters','Position', [left top w*1.5 w]);
    end
    
    dn = round(length(t_snapshot)/200);
    
    for idx = 1:dn:length(t_snapshot)
        axes(axs);
        PlotBirdView(reshape(y_snapshot(:,idx),n,n),markalive_snapshot(:,idx));        
        %title(t_snapshot(idx));
        if(t_snapshot(idx) < 0)
            title(['Initial'],'FontWeight','Normal');                                
        else
            title([num2str(round(t_snapshot(idx))),' hours'],'FontWeight','Normal');        
        end        
        colormap(axs(1),brewermap(9,'Purples'));
        caxis(axs(1),[0 1]);
        

        colormap(axs_colorbar,brewermap(9,'Purples'));    
        h = colorbar(axs_colorbar,'XTick',[0,1]);
        h.Label.String = 'Acute damage x';
        h.Box = 'off';
        set(h,'FontSize',12,'FontName','Arial','FontWeight', 'Normal');
        set(axs_colorbar,'XColor','none');
        set(h, 'YAxisLocation','left');
        caxis([0 1]);
        set(axs_colorbar,'Visible','off');
        
        
         for ax = axs
            set(ax,'FontSize',12,'FontName','Arial','FontWeight', 'Normal'); 
        end
        for ax = axs_colorbar
            set(ax,'FontSize',12,'FontName','Arial','FontWeight', 'Normal');  
        end

        drawnow 

        % Capture the plot as an image 
        frame = getframe(hP); 
        im = frame2im(frame); 
        [imind,cm] = rgb2ind(im,256); 
        % Write to the GIF File 
        if idx == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.05); 
        else 
          imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.05); 
        end 
        
    end    
end