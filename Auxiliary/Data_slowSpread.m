function results= Data_slowSpread(params)

    %**************************************************
    %Parameters    
    %**************************************************    
    z0_mean = params.z0_mean;
    z0_std = params.z0_std;
    tau_inf = params.tau_inf;
    n = params.n;
    M = params.M;
    Tstart = params.Tstart;
    Tmax = params.Tmax;
    dt = params.dt;
    seed = params.seed;      
    
    %**************************************************
    %Initialization;    
    %**************************************************    
    rng(seed);
    N  = n^2;
    ts = Tstart:dt:Tmax;
    A = InitializeConnectivityMatrices_A(n,M);
    x_1D  = 1:n;
    [x1,x2] = meshgrid(x_1D,x_1D); 
    z = LogNormal(z0_mean,z0_std,N);
    [~,idx_seed] = min((x1(:)-n/2).^2+(x2(:)-n/2).^2);    
    mark_alive = true(N,1);
    z_snapshot = [];
    t_snapshot = []; 
    n_dead = [];
    n_seeds = [];
    markalive_snapshot = []; 
    
    [~,i__seed] = min(abs(ts-0));
    
    %**************************************************
    %Simulation
    %**************************************************   
    for i__ = 1:length(ts)
        if(i__ == i__seed)
            z(idx_seed) = z(idx_seed) + 0.5;
        end
        dz             = f(z);         
        z(mark_alive)  = z(mark_alive) + dz(mark_alive);        
        mark_dead_new             = ((z >= 1) & mark_alive);                
        mark_alive(mark_dead_new) = false;        
        
        if(mod(i__,10)==1)
            t_snapshot(end+1) = ts(i__);
            z_snapshot(:,end+1) = z;
            n_dead(end+1) = sum(~mark_alive);
            n_seeds(end+1) = sum(mark_alive & (z >= 0.5));
            markalive_snapshot(:,end+1) = mark_alive;
        end
    end

    results = v2struct(t_snapshot,z_snapshot,markalive_snapshot,n_seeds);
    
    function dz = f(z)
        seeds                              = zeros(N,1);
        seeds((z >= 0.5) & mark_alive)     = 1;
                
        dz                     = A*seeds*dt/tau_inf;                                          
     end 
end