function Fig_SlowSpread(results,params)%,axs,axs_colorbar,axs_graphs,recordVideo)
    
    global dirData

    plot_times = params.plot_times;  
    n          = params.n;
    Tstart     = params.Tstart;
    Tmax       = params.Tmax;
    
    
    z_snapshot         = results.z_snapshot;
    markalive_snapshot = results.markalive_snapshot;
    t_snapshot         = results.t_snapshot;
    n_seeds            = results.n_seeds;
    
    %**************************************************
    %Plotting
    %************************************************** 
    figure('Units', 'centimeters','Position', [0, 0, 18,3.5],...
       'PaperUnits', 'centimeters', 'PaperSize', [18,3.5],...
       'PaperPositionMode','Auto');
   
    left_alignment = 1;
    top_alignment = 1;
    axs = [];
    axs_colorbar = [];
    axs_ana = [];
    w = 2;
    w_spacing = 2.2;
    w_spacing_vertical = 2.5;
    for i1 = 1:1   
        top  = top_alignment - w_spacing_vertical*(i1-1);

        left = 1;
        axs_colorbar(i1) = axes('Units', 'centimeters','Position', [left top w*0.2 w]);    

        for i2 = 1:5
            left = left_alignment + w_spacing*(i2-1) + 1.;    
            axs(i1,i2) = axes('Units', 'centimeters','Position', [left top w w]);
        end        

        left = left_alignment + w_spacing*5+2.2;
        axs_ana(i1)  = axes('Units', 'centimeters','Position', [left top w*1.5 w]);
    end

    
    for i_ = 1:length(plot_times)
        
        axes(axs(i_));
        [~,idx] = min(abs(t_snapshot-plot_times(i_)));               
                
        PlotBirdView(reshape(z_snapshot(:,idx),n,n),markalive_snapshot(:,idx));        
        
        if(t_snapshot(idx) < 0)
            title(['Initial'],'FontWeight','Normal');                                
        else
            title([num2str(t_snapshot(idx)),' years'],'FontWeight','Normal');        
        end        
        colormap(axs(i_),brewermap(9,'Reds'));
        caxis(axs(i_),[0 0.5]);
    end
    
    colormap(axs_colorbar,brewermap(9,'Reds'));    
    h = colorbar(axs_colorbar,'XTick',[0,0.5,1]);
    h.Label.String = 'Baseline damage z';
    h.Box = 'off';
    set(h,'FontSize',7,'FontName','Arial','FontWeight', 'Normal');
    set(axs_colorbar,'XColor','none');
    set(h, 'YAxisLocation','left');
    caxis(axs_colorbar,[0 1]);
    set(axs_colorbar,'Visible','off');
    
    axes(axs_ana);    
    plot(t_snapshot,n_seeds,'k-');  hold on;    
    xlim([Tstart,Tmax]);
     
    xlabel('Time [years]');
    ylabel('# seeds');
    YL = ylim();    
    plot([0,0],YL,'k--','linewidth',1);
    h_ = text(0,YL(2)/2,'Seed induction','FontSize',7,'FontName',...
                        'Arial','FontWeight', 'Normal',...
                        'HorizontalAlignment', 'center',...
                        'VerticalAlignment', 'bottom'); %'FontSize',14
    set(h_,'Rotation',90);    
    xticks(plot_times);
    
     
    for ax = axs
        set(ax,'FontSize',7,'FontName','Arial','FontWeight', 'Normal'); 
    end
    for ax = axs_colorbar
        set(ax,'FontSize',7,'FontName','Arial','FontWeight', 'Normal');  
    end

    for ax = axs_ana
        set(ax,'FontSize',7,'FontName','Arial','FontWeight', 'Normal');  
    end

    print(gcf,[dirData,'Fig1_SlowSpread'],'-dpdf','-r0');
 
end