close all;
clearvars;

global dirData            
dirData = 'Output/';
if ~exist(dirData, 'dir')
   mkdir(dirData)
end
addpath(genpath(pwd));        


global recompute
recompute = true;
if(recompute)
    disp('!!! Data will be recomputed. recomputeAll = true !!!');
end        
