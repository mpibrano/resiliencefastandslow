AddPaths;


%**************************************************
%Parameters    
%**************************************************    
z0_mean   = 0.05; 
z0_std    = 0.02;
tau_inf   = 10; 

n      = 20; 
M      = 10;
Tstart = -3;
Tmax = 11;     
dt   = 0.01; 
seed = 1;

plot_times =[-5,0,3,6,9];

params = v2struct(z0_mean,z0_std,tau_inf,n,M,Tstart,Tmax,dt,seed,plot_times);

%************************************
% Compute 
%************************************
results            = Data_slowSpread(params);
%**************************************************
%Plot    
%**************************************************   
Video_SlowSpread(results,params);
Fig_SlowSpread(results,params);
%************************************

