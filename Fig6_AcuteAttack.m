AddPaths;

params = struct('z0_mean',0.075,'z0_std',0.035,...
                 'c',0.55,'D',-2,'G',3,...
                 't_plot',[-20,60,70,80,120],'n',30,...
                 'dt',0.005,'seed',1,'tau',4,'tau_death',24,'seed_start',-0.01);                        
                       
snapshot_data = Data_shortTermDynamics(params);
Video_ShortTermDynamics(snapshot_data,'Video6_AcuteAttack_PartialKnockout.gif');

params = struct('z0_mean',0.075,'z0_std',0.035,...
                 'c',0.55,'D',0,'G',3,...
                 't_plot',[-20,60,70,80,85],'n',30,...
                 'dt',0.005,'seed',1,'tau',4,'tau_death',24,'seed_start',-0.01);                        
                       
snapshot_data = Data_shortTermDynamics(params);

%*********************************
% Video
%*********************************
Video_ShortTermDynamics(snapshot_data,'Video6_AcuteAttack.gif');

%*********************************
% Plotting
%*********************************

fig1 = figure('Units', 'centimeters','Position', [0, 0, 18,8.5],...
       'PaperUnits', 'centimeters', 'PaperSize', [18,8.5],...
       'PaperPositionMode','Auto');
   
   
left_alignment = 1;
top_alignment = 6;
axs = [];
axs_colorbar = [];
axs_ans = [];
w = 2;
w_spacing = 2.2;
w_spacing_vertical = 2.5;
for i1 = 1:3   
    top  = top_alignment - w_spacing_vertical*(i1-1);

    left = 1;
    axs_colorbar(i1) = axes('Units', 'centimeters','Position', [left top w*0.2 w]);    
    
    for i2 = 1:5
        left = left_alignment + w_spacing*(i2-1) + 1.;    
        axs(i1,i2) = axes('Units', 'centimeters','Position', [left top w w]);
    end
    left = left_alignment + w_spacing*5+2.2;
    axs_ana(i1)  = axes('Units', 'centimeters','Position', [left top w*1.5 w]);
end   

%************************************
% Compute 
%************************************
 
Fig_shortTermDynamics(snapshot_data,axs,axs_colorbar,axs_ana);

%************************************
%
%************************************

for ax = axs
    set(ax,'FontSize',7,'FontName','Arial','FontWeight', 'Normal'); 
end
for ax = axs_colorbar
    set(ax,'FontSize',7,'FontName','Arial','FontWeight', 'Normal');  
end

for ax = axs_ana
    set(ax,'FontSize',7,'FontName','Arial','FontWeight', 'Normal');  
end

fig1.Renderer='Painters';
print(gcf,[dirData,'Fig6_AcuteAttack'],'-dpdf','-r0');
