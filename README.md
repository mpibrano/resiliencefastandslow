# ResilienceFastAndSlow

This is the code to reproduce the figures of the main text of the manuscript "How repair-or-dispose decisions under stress can initiate disease progression" 
by Andreas Nold, Danylo Batulin, Katharina Birkner, Stefan Bittner and Tatjana Tchumatchenko on 
[BioRxiv](https://www.biorxiv.org/content/10.1101/828053v2).

The code was tested with Matlab R2018a.

